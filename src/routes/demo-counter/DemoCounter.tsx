import { useDispatch, useSelector } from 'react-redux';
import { ShippingFiltersPanel } from './components/ShippingFiltersPanel.tsx';
import * as CounterActions from './store/counter/counter.actions.ts';
import * as ShippingActions from './store/shipping/shipping.actions.ts';
import { selectBoxes, selectTotalProducts } from './store/counter/counter.selectors.ts';
import { selectItemsPerBox, selectMaterial } from './store/shipping/shipping.selectors.ts';

export function DemoCounter() {
  const dispatch = useDispatch()

  const totalProducts = useSelector(selectTotalProducts)
  const material = useSelector(selectMaterial)
  const itemsPerBox = useSelector(selectItemsPerBox)
  const boxes = useSelector(selectBoxes)

  return <div>
    <h1>Demo Counter </h1>

    <div>totalProducts: {totalProducts}</div>
    <div>{boxes} boxes of {material} ({itemsPerBox} per box)</div>

    <div>
      <button onClick={() => dispatch(CounterActions.decrement(5))}>-</button>
      <button onClick={() => dispatch(CounterActions.increment(10))}>+</button>
      <button onClick={() => dispatch(CounterActions.reset())}>reset</button>
      <hr/>

      {/*<button onClick={() => dispatch(ShippingActions.changeMaterial('plastic'))}>Plastic</button>
      <button onClick={() => dispatch(ShippingActions.changeMaterial('wood'))}>Wood</button>*/}
    </div>

    -----
    <ShippingFiltersPanel
      onWoodSelect={() => dispatch(ShippingActions.changeMaterial('wood'))}
    />

  </div>
}
