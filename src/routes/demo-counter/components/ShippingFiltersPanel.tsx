import { useDispatch } from 'react-redux';
import * as ShippingActions from '../store/shipping/shipping.actions.ts';

interface ShippingFiltersPanelProps {
  onWoodSelect: () => void;
}
export function ShippingFiltersPanel(props: ShippingFiltersPanelProps) {
  const dispatch = useDispatch()

  return (

    <div>
      <button onClick={() => dispatch(ShippingActions.changeMaterial('plastic'))}>Plastic</button>
      <button onClick={() => dispatch(ShippingActions.changeMaterial('wood'))}>Wood</button>

      <button onClick={props.onWoodSelect}>Wood</button>
    </div>
  )
}
