import { combineReducers } from '@reduxjs/toolkit';
import { counterReducer } from './counter/counter.reducer.ts';
import { shippingReducer } from './shipping/shipping.reducer.ts';

export const orderReducer = combineReducers({
  counter: counterReducer,
  shipping: shippingReducer
})
