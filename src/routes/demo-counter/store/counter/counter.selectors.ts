import { RootState } from '../../../../main.tsx';

export const selectTotalProducts = (state: RootState) => state.order.counter.value

export const selectBoxes = (state: RootState) => {
  return Math.ceil(state.order.counter.value / state.order.shipping.itemsPerBox);
}
