import { createReducer } from '@reduxjs/toolkit';
import { decrement, increment, reset } from './counter.actions.ts';

export type ConfigState =  {
  value: number;
}

const initialState: ConfigState = {
  value: 0,
}

export const counterReducer = createReducer(initialState, builder => builder
  .addCase(increment, (state, action) => ({ ...state, value: state.value + action.payload }))
  .addCase(decrement, (state, action) => {
    if (state.value > 0) {
      state.value -= action.payload;
    }
  })
  .addCase(reset, () => initialState)
)


/*
// 2. deprecated
export const counterReducer = createReducer(0, {
  'increment': (state: any, action: any ) => state + 1,
  'decrement': (state: any, action: any ) => state - 1,
})*/

/*
// 1) OLD SCHOOL WAY
export function counterReducer(state = 0, action: any) {
  switch (action.type) {
    case 'increment':
      return state + 1;
    case 'decrement':
      return state - 1;
    default:
      return state
  }
}
*/
