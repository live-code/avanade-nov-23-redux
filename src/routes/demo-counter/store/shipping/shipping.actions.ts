import { createAction } from '@reduxjs/toolkit';

export type Material = 'wood' | 'plastic';

export const updateItemsPerBox = createAction<5 | 10>('updateItemsPerBox')
export const changeMaterial = createAction<Material>('changeMaterial')
