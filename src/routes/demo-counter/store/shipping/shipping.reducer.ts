import { createReducer } from '@reduxjs/toolkit';
import { changeMaterial, Material, updateItemsPerBox } from './shipping.actions.ts';

export type ConfigState =  {
  itemsPerBox: number;
  material: Material
}

const initialState: ConfigState = {
  itemsPerBox: 5,
  material: 'wood'
}

export const shippingReducer = createReducer(initialState, builder => builder
  .addCase(updateItemsPerBox, (state, action) => {
    state.itemsPerBox = action.payload;
  })
  .addCase(changeMaterial, (state, action) => {
    state.material = action.payload;

    if (action.payload === 'wood') {
      state.itemsPerBox = 5;
    } else {
      state.itemsPerBox = 10;
    }
  })

)


/*
// 2. deprecated
export const counterReducer = createReducer(0, {
  'increment': (state: any, action: any ) => state + 1,
  'decrement': (state: any, action: any ) => state - 1,
})*/

/*
// 1) OLD SCHOOL WAY
export function counterReducer(state = 0, action: any) {
  switch (action.type) {
    case 'increment':
      return state + 1;
    case 'decrement':
      return state - 1;
    default:
      return state
  }
}
*/
