import { RootState } from '../../../../main.tsx';

export const selectMaterial = (state: RootState) => state.order.shipping.material
export const selectItemsPerBox = (state: RootState) => state.order.shipping.itemsPerBox
