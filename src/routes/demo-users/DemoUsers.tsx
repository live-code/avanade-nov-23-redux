import { useState } from 'react';
import { useGetUsersQuery, useGetUserByIdQuery, useDeleteUserMutation } from './store/users.api.ts';

export function DemoUsers() {
  const { data, error, isLoading } = useGetUsersQuery()
  const [, setActiveId] = useState(1)

  const [deleteFn, { isLoading: isDeleteLoading }] = useDeleteUserMutation();


  if (error) {
    return <div>Ahia! errore lato server</div>
  }


  return <div>
    Demo Users

    {(isLoading || isDeleteLoading)  && <div>loading...</div>}

    {
      data?.map(item => {
        return <li
          onClick={() => setActiveId(item.id)}
          key={item.id}>
          {item.name}
          <button onClick={() => deleteFn(item.id)}>delete</button>
        </li>
      })
    }

    <hr/>

    {/*<SingleUser id={activeId} />*/}
  </div>
}

export function SingleUser(props: { id: number}) {
  const { data, error, isLoading } = useGetUserByIdQuery(props.id)

  if (error) {
    return <div>Ahia! errore lato server</div>
  }

  if (isLoading) {
    return <div>loading...</div>
  }

  return <div>
    Single User {props.id}

    <pre>{JSON.stringify(data, null, 2)}</pre>
  </div>
}


