import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { User } from '../model/user.ts';

export const usersApi = createApi({
  reducerPath: 'users',
  baseQuery: fetchBaseQuery({ baseUrl: 'http://localhost:3000/'}),
  tagTypes: ['Users'],
  // refetchOnMountOrArgChange: true,
  endpoints: (builder) => ({
    getUsers: builder.query<User[], void>({
      query: () => 'users',
      providesTags: ['Users'],
     // keepUnusedDataFor: 10,
    }),
    getUserById: builder.query<User, number>({
      query: (id) => `users/${id}`
    }),
    search: builder.query<User[], string>({
      query: (text) => `users?q=${text}`
    }),
    deleteUser: builder.mutation({
      query: (id) => ({
        url: `users/${id}`,
        method: 'DELETE'
      }),
      invalidatesTags: ['Users']
    })
  })
})

export const {
  useGetUserByIdQuery, useGetUsersQuery, useSearchQuery,
  useDeleteUserMutation, usePrefetch
} = usersApi
