import React, { useEffect, useRef } from 'react';
import { useSelector } from 'react-redux';
import { useAppDispatch } from '../../main.tsx';
import { selectError, selectList, selectPending } from './store/filters/products-filters.selectors.ts';
import { setTextFilter } from './store/filters/products-filters.store.ts';
import { addProduct, deleteProduct, getProducts } from './store/products/products.actions.ts';
import { clear } from './store/products/products.store.ts';


export function DemoCatalog() {
  const dispatch = useAppDispatch();
  const products = useSelector(selectList)
  const error = useSelector(selectError)
  const pending = useSelector(selectPending)
  const timer = useRef<number>()

  useEffect(() => {
    dispatch(getProducts())

    return () => {
      dispatch(clear())
    }
  }, []);

  function addTodoHandler(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      dispatch(addProduct({
        title: e.currentTarget.value,
        price: 10,
      }))
      e.currentTarget.value = '';
    }
  }


  function onTextFilterHandler(e: React.ChangeEvent<HTMLInputElement>) {
    const value = e.currentTarget.value;

    clearTimeout(timer.current);
    timer.current = setTimeout(() => {
      dispatch(setTextFilter(value))
    }, 1000)
  }

  return <div>
    <h1>Catalog</h1>

    {error && <div>{error}</div>}
    {pending && <div>loading....</div>}

    <input type="text" placeholder="Search"
           onChange={onTextFilterHandler}
    />

    {
      products.map(p => {
        return <li key={p.id}>
          {p.title}
          <button onClick={() => dispatch(deleteProduct(p.id))}>del</button>
        </li>
      })
    }

    <h1>Add new Product</h1>
    <input type="text" onKeyDown={addTodoHandler} placeholder="add new"/>

  </div>
}
