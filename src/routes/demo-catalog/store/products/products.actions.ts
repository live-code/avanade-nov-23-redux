import axios from 'axios';
import { hideError, showError } from '../../../../core/store/error.store.ts';
import { AppThunk } from '../../../../main.tsx';
import { Product } from '../../model/product.ts';
import {
  addProductAction,
  addProductSuccess,
  deleteProductSuccess,
  getProductsSuccess,
  setError, setPending
} from './products.store.ts';

export const getProducts2 = function (): AppThunk {
  return async function(dispatch) {
    const res = await axios.get<Product[]>(`http://localhost:3000/products`);
    dispatch(getProductsSuccess(res.data))
  }
}

export const getProducts = (): AppThunk => (dispatch) => {
  dispatch(setPending());
  dispatch(hideError())
  axios.get<Product[]>(`http://localhost:3000/products`)
    .then(res=> {
      dispatch(getProductsSuccess(res.data))
    })
    .catch((err) => {
      dispatch(setError(' load failed'))
      dispatch(showError({ status: err.status, msg: 'load products failed'}))
    })
}

export const addProduct = (productToAdd: Omit<Product, 'id' | 'visibility'>): AppThunk => async (dispatch) => {
  dispatch(addProductAction(productToAdd))
  dispatch(hideError())
  dispatch(setPending());

  try {
    const res = await axios.post<Product>(`http://localhost:3000/products/`, {
      ...productToAdd,
      visibility: false
    });
    dispatch(addProductSuccess(res.data))
  } catch(err: any) {
    dispatch(setError(' add failed '))
    dispatch(showError({ status: err.status, msg: 'add products failed'}))
  }

}

export const deleteProduct = (id: number): AppThunk => async (dispatch) => {
  await axios.delete(`http://localhost:3000/products/${id}`);
  dispatch(deleteProductSuccess(id))
}
