import { createAction, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { Product } from '../../model/product.ts';

export const addProductAction =
  createAction<Omit<Product, 'id' | 'visibility'>>('addProduct')


export type ProductsState = {
  list: Product[];
  error: string | null;
  pending: boolean;
}

const initialState: ProductsState = {
  list: [],
  error: null,
  pending: false
}

export const productsStore = createSlice({
  name: 'products',
  initialState,
  reducers: {
    setPending(state) {
      state.pending = true;
      state.error = null;
    },
    setError(state, action: PayloadAction<string>) {
      state.error = action.payload;
      state.pending = false;
    },
    getProductsSuccess(state, action: PayloadAction<Product[]>) {
       state.list = action.payload;
       state.pending = false;
      // return action.payload;
    },
    addProductSuccess(state, action: PayloadAction<Product>) {
      state.list.push(action.payload)
      state.pending = false;
      //return [...state, action.payload]
    },
    deleteProductSuccess(state, action: PayloadAction<number>) {
      const index = state.list.findIndex(p => p.id === action.payload)
      state.list.splice(index, 1)
      state.pending = false;
    },
    toggleProductVisibility(state, action: PayloadAction<number>) {
      const product = state.list.find(p => p.id === action.payload);
      if (product) {
        product.visibility = !product.visibility;
      }
      state.pending = false;
    },
    clear(state) {
      state.list = [];
      state.error = null;
      state.pending = false
    }
  }
});

export const {
  addProductSuccess, getProductsSuccess, toggleProductVisibility, deleteProductSuccess, setError,
  setPending, clear
} = productsStore.actions;
