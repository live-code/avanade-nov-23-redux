import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export type FilterState = {
  text: string;
}

const initialState: FilterState = {
  text: ''
}
export const productsFiltersStore = createSlice({
  name: 'productsFilter',
  initialState,
  reducers: {
    setTextFilter(state, action: PayloadAction<string>) {
      state.text = action.payload;
    },
    clear(state) {
      state.text = ''
    }
  }
})

export const {
  setTextFilter, clear
} = productsFiltersStore.actions
