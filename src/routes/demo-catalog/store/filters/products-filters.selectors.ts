import { RootState } from '../../../../main.tsx';

export const selectList = (state: RootState) => {
  // ...
  return state.catalog.products.list.filter(item => item.title.toLowerCase().includes(state.catalog.filters.text.toLowerCase()))
}
export const selectError = (state: RootState) => state.catalog.products.error
export const selectPending = (state: RootState) => state.catalog.products.pending
