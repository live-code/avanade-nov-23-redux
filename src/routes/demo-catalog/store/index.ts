import { combineReducers } from '@reduxjs/toolkit';
import { productsFiltersStore } from './filters/products-filters.store.ts';
import { productsStore } from './products/products.store.ts';

export const catalogReducer = combineReducers({
  products: productsStore.reducer,
  filters: productsFiltersStore.reducer,
})
