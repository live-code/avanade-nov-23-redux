import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { News } from '../model/news.ts';

const initialState: News[] = [];

export const newsStore = createSlice({
  name: 'news',
  initialState,
  reducers: {
    addNews(state, action: PayloadAction<string> ) {
      /*const newNews = { id: Date.now(), title: action.payload, published: false, }
      return [...state, newNews]*/
      state.push({
        id: Date.now(),
        title: action.payload,
        published: false,
      })
    },
    deleteNews(state, action: PayloadAction<number>) {
      // return state.filter(item => item.id !== action.payload)
      const index = state.findIndex(item => item.id === action.payload)
      state.splice(index, 1)

    },
    toggleNewsVisibility(state, action: PayloadAction<number>) {
      const newsToUpdate = state.find(item => item.id === action.payload)
      if (newsToUpdate)
        newsToUpdate.published = !newsToUpdate.published
    },
    editNews(state, action: PayloadAction<{ id: number, title: string }>) {
      const newsToUpdate = state.find(item => item.id === action.payload.id)
      if (newsToUpdate)
        newsToUpdate.title = action.payload.title
    },

  }
})

export const {
  addNews,
  deleteNews,
  toggleNewsVisibility,
  editNews
} = newsStore.actions
