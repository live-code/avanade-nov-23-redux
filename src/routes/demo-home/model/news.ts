export type News = {
  id: number;
  title: string;
  published: boolean;
}
