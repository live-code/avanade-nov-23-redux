import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../main.tsx';
import { addNews, deleteNews, editNews, toggleNewsVisibility } from './store/news.store.ts';

export function DemoHome() {
  const dispatch = useDispatch();
  const newsList = useSelector((state: RootState) => state.news)
  const total = useSelector((state: RootState) => state.news.length)

  function onKeyDownHandler(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      dispatch(addNews(e.currentTarget.value))
    }
  }

  function updateNews(e: React.ChangeEvent<HTMLInputElement>, id: number) {
    dispatch(editNews({ id, title: e.currentTarget.value }))
  }

  return <div>
    <h1>DemoHome</h1>

    <input type="text" onKeyDown={onKeyDownHandler} placeholder="add news title" />

    <div>
    total items: {total}

    {
      newsList.map(item => {

        return <li key={item.id}>
          <input type="text" defaultValue={item.title}
                 onBlur={(e) => updateNews(e, item.id)}/>

          <button onClick={() => dispatch(deleteNews(item.id))}>del</button>
          <button
            onClick={() => dispatch(toggleNewsVisibility(item.id))}
          >{item.published ? 'PUBLISHED' : 'DRAFT'}</button>
        </li>
      })
    }
    </div>
  </div>
}
