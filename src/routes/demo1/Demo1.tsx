import { useDispatch } from 'react-redux';
import { changeTheme } from '../../core/store/config.slice.ts';

export function Demo1() {
  const dispatch = useDispatch()
  return (
    <>

      <h1>Demo 1</h1>

      <div className="flex gap-2">
        <button onClick={() => dispatch(changeTheme('dark'))}>Dark</button>
        <button onClick={() => dispatch(changeTheme('light'))}>Light</button>
      </div>
    </>
  )
}

