import {  combineReducers, configureStore, ThunkAction, ThunkDispatch, UnknownAction } from '@reduxjs/toolkit';
import ReactDOM from 'react-dom/client'
import { Provider, useDispatch } from 'react-redux';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import App from './App.tsx'
import './index.css'
import { errorStore } from './core/store/error.store.ts';
import { DemoCatalog } from './routes/demo-catalog/DemoCatalog.tsx';
import { catalogReducer } from './routes/demo-catalog/store';
import { DemoCounter } from './routes/demo-counter/DemoCounter.tsx';
import { orderReducer } from './routes/demo-counter/store';
import { DemoHome } from './routes/demo-home/DemoHome.tsx';
import { newsStore } from './routes/demo-home/store/news.store.ts';
import { DemoUsers } from './routes/demo-users/DemoUsers.tsx';
import { usersApi } from './routes/demo-users/store/users.api.ts';
import { Demo1 } from './routes/demo1/Demo1.tsx';
import { configStore } from './core/store/config.slice.ts';


const rootReducer = combineReducers({
  order: orderReducer,
  config: configStore.reducer,
  news: newsStore.reducer,
  catalog: catalogReducer,
  error: errorStore.reducer,
  [usersApi.reducerPath]: usersApi.reducer
})

export const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware()
    .concat([usersApi.middleware])
})

export type RootState = ReturnType<typeof rootReducer>
export type AppThunk = ThunkAction<void, RootState, null, UnknownAction>
export type AppDispatch = ThunkDispatch<RootState, null, UnknownAction>;
export const useAppDispatch = () => useDispatch<AppDispatch>()
/*
export type RootState = {
  todos: number[],
  config: ConfigState,
  order: {
    counter: ...
}*/


const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    children: [
      { path: 'demo-catalog', element: <DemoCatalog />},
      { path: 'demo-counter', element: <DemoCounter />},
      { path: 'demo-home', element: <DemoHome />},
      { path: 'demo-users', element: <DemoUsers/>},
      { path: 'demo-helloworld', element: <Demo1 />},
    ]
  }
])



ReactDOM.createRoot(document.getElementById('root')!).render(
  <Provider store={store}>
    <RouterProvider router={router} />
  </Provider>
)
/*


ReactDOM.createRoot(document.getElementById('root')!).render(
  <Provider store={store}>
    <App />
  </Provider>
)
*/
