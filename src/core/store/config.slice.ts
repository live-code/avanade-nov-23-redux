import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export type Language = 'it' | 'en';
export type Theme = 'dark' | 'light';

export type ConfigState = {
  theme: Theme;
  lang: Language
}

const initialState: ConfigState = {
  theme: 'light',
  lang: 'it'
}

export const configStore = createSlice({
  name: 'config',
  initialState,
  reducers: {
    changeLanguage(state, action: PayloadAction<Language>) {
      state.lang = action.payload
      // return { ...state, lang: action.payload}
    },
    changeTheme(state, action: PayloadAction<Theme>) {
      state.theme = action.payload
      // return { ...state, lang: action.payload}
    }
  }
})

export const {
  changeLanguage, changeTheme
} = configStore.actions
