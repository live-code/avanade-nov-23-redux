import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export type ErrorState = {
  status: number | null;
  msg: string;
}

const initialState: ErrorState = {
  status: null,
  msg: ''
}
export const errorStore = createSlice({
  name: 'error',
  initialState,
  reducers: {
    showError(state, action: PayloadAction<ErrorState>) {
      state.msg = action.payload.msg
      state.status = action.payload.status

      // return action.payload
    },
    hideError(state) {
      state.msg = ''
      state.status = null
    }
  }
})

export const {
  showError, hideError
} = errorStore.actions
