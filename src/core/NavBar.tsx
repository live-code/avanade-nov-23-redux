import clsx from 'clsx';
import { useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { RootState } from '../main.tsx';

export function NavBar() {
  const theme = useSelector((state: RootState) => state.config.theme)
  const globalError = useSelector((state: RootState) => state.error.msg)

  return <div className={clsx({
    'bg-black text-white': theme === 'dark',
    'bg-slate-200 text-black': theme === 'light',
  })}>

    {globalError && <div className="bg-red-500">{globalError}</div>}

    <h1>{theme}</h1>
    <div className="flex gap-3">
      <NavLink to="demo-helloworld">helloworld</NavLink>
      <NavLink to="demo-counter">counter</NavLink>
      <NavLink to="demo-catalog">catalog</NavLink>
      <NavLink to="demo-home">home</NavLink>
      <NavLink to="demo-users">users</NavLink>
    </div>
  </div>
}
