import { Outlet } from 'react-router-dom';
import { NavBar } from './core/NavBar.tsx';

function App() {

  return (
    <>

      <NavBar />
      <hr/>

      <Outlet />
    </>
  )
}

export default App
